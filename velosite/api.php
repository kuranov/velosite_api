<?php
/**
 * User: kuranov
 * Date: 25.03.2018
 * Time: 0:49
 */

namespace Velosite;

class Api {

    const API_URL = "https://api.velosite.ru";

    const API_VERSION = "v1.2";

    private $apiUser = null;

    private $apiPassword = null;


    function __construct($username, $password)
    {
        $this->apiUser = $username;
        $this->apiPassword = $password;
    }

    /**
     * Осуществление API-запроса
     * @param string $apiMethod принимает либо название метода относительно адреса API (прим «orders»), либо полный URL для методов поддерживающих пагинацию
     * @param string $method метод HTTP-запроса
     * @param mixed $data
     * @return ApiResponse
     */
    public function call($apiMethod, $method = "GET", $data = false)
    {
        if ($this->isDirectUrl($apiMethod))
        {
            $url = $this->getMethodUrl($apiMethod, true);
            $method = "GET";
        }
        else
        {
            $url = $this->getMethodUrl($apiMethod);
        }

        $curl = curl_init();

        switch ($method)
        {
            case "POST":
                curl_setopt($curl, CURLOPT_POST, 1);

                if ($data)
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
                break;
            case "PUT":
                curl_setopt($curl, CURLOPT_PUT, 1);
                break;
            case "PATCH":
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'PATCH');
                curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
                curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
                break;
            default:
                if ($data)
                    $url = sprintf("%s?%s", $url, http_build_query($data));
        }

        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        curl_setopt($curl, CURLOPT_USERPWD, implode(":", array($this->apiUser, $this->apiPassword)));
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

        $result = curl_exec($curl);
        $response = new ApiResponse($result, $curl);

        curl_close($curl);

        return $response;
    }

    /**
     * @param $method
     * @param bool $direct
     * @return string
     */
    public function getMethodUrl($method, $direct = false)
    {
        if ($direct)
        {
            return self::API_URL . $method;
        }

        return self::API_URL . "/" . self::API_VERSION . "/" . $method;
    }

    /**
     * Определяет является ли переданный url полной ссылкой на осушествления запроса в API (для методов пагинации)
     * Пример: /v1.1/sku?page%5Bsize%5D=20&page%5Bnumber%5D=2
     * @param string $url
     * @return bool
     */
    public function isDirectUrl($url)
    {
        return preg_match("/^\/".preg_quote(self::API_VERSION)."/", $url);
    }

    /**
     * Получение списка товаров
     * GET /sku
     * https://velosite.atlassian.net/wiki/spaces/VPA/pages/53870639
     *
     * @param int $pageNumber
     * @param int $pageSize
     * @return ApiResponse
     */
    public function getSku($pageNumber = 1, $pageSize = 10)
    {
        $params = array(
            "page" => array(
                "number" => $pageNumber,
                "size" => $pageSize
            )
        );

        return $this->call("sku", "GET", $params);
    }


    /**
     * Получение списка активных производителей. Метод не поддерживает пагинацию
     * GET /vendors
     * https://velosite.atlassian.net/wiki/spaces/VPA/pages/51544099
     *
     * @return ApiResponse
     */
    public function getVendors()
    {
        return $this->call("vendors");
    }

    /**
     * Получение списка категорий товаров. Метод не поддерживает пагинацию
     * GET /categories
     * https://velosite.atlassian.net/wiki/spaces/VPA/pages/51413030
     *
     * @return ApiResponse
     */
    public function getCategories()
    {
        return $this->call("categories");
    }

    /**
     * Получение списка заказов
     * GET /orders
     * https://velosite.atlassian.net/wiki/spaces/VPA/pages/53968900
     *
     * @param string $status Фильтрация заказов по статусу, принимает одно из значений: https://velosite.atlassian.net/wiki/spaces/VPA/pages/53903384
     * @param int $pageNumber
     * @param int $pageSize
     * @return ApiResponse
     */
    public function getOrders($status = null, $pageNumber = 1, $pageSize = 10)
    {
        $params = array(
            "page" => array(
                "number" => $pageNumber,
                "size" => $pageSize
            )
        );
        if ($status)
        {
            $params["status"] = $status;
        }

        return $this->call("orders", "GET", $params);
    }

    /**
     * Создание заказа
     *
     * @param $data
     * @return ApiResponse
     */
    public function createOrder($data)
    {
        return $this->call("orders", "POST", $data);
    }

    public function getReferencesOrderStates()
    {
        return $this->call("references/order-states");
    }

    public function getReferencesOrderPaymentStates()
    {
        return $this->call("references/order-payment-states");
    }


    public function getReferencesDeliveryTypes()
    {
        return $this->call("references/delivery-types");
    }


    public function getReferencesPaymentTypes()
    {
        return $this->call("references/payment-types");
    }
}

class ApiResponse {

    const ERROR_INVALID_RESPONSE = "Invalid JSON in response";
    const ERROR_EMPTY_RESPONSE = "Empty JSON response";

    private $data = null;

    private $httpStatus = null;

    private $links = null;

    private $error = null;

    function __construct($data = null, $curlHandler = null)
    {
        if ($data != null)
        {
            $this->setData($data);
        }

        if ($curlHandler != null)
        {
            $this->extractCurlInfo($curlHandler);
        }
    }

    function extractCurlInfo($curlHandler)
    {
        $error = curl_error($curlHandler);
        if ($error)
        {
            $this->error = array(
                "message" => $error,
                "code" => curl_errno($curlHandler)
            );
        }

        $responseInfo = curl_getinfo($curlHandler);
        $this->httpStatus = $responseInfo["http_status"];
    }

    function setData($rawJson)
    {
        $this->data = null;
        $this->links = null;
        $this->error = null;

        if (empty($rawJson))
        {
            $this->error = array(
                "message" => self::ERROR_EMPTY_RESPONSE
            );
            return;
        }

        $json = json_decode($rawJson);

        if (!$json)
        {
            $this->error = array(
                "message" => self::ERROR_INVALID_RESPONSE
            );
            return;
        }

        $this->data = $json->data;


        if (isset($json->links))
        {
            $this->links = $json->links;
        }
    }

    function getData()
    {
        return $this->data;
    }

    function getNextPage()
    {
        if ($this->links == null || isset($this->links->next)) {
            return null;
        }
        return $this->links->next;
    }

    function getHttpStatus()
    {
        return $this->httpStatus;
    }

    function getError()
    {
        return $this->error;
    }

    function isSuccess()
    {
        return !$this->getError() && $this->getHttpStatus() == 200 && !empty($this->getData());
    }
}